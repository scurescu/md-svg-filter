const fs = require('fs-extra')
const { lstatSync, readdirSync } = require('fs')
const moveFile = require('move-file');
const {join} = require('path')

/* RegEx to match svg size */
const regEx = /(.*)24px.svg/gm

/* Directory name for md repository */
const mdRepo = 'material-design-icons'

/* @TODO: Automaticly remove unusfull folders */
// const uslessFolders = [`${mdRepo}/.git`, `${mdRepo}/iconfont`, `${mdRepo}/sprites`]

/* Filter directories */
const isDirectory = source => lstatSync(source).isDirectory()
const getDirectories = source => readdirSync(source).map(name => join(source, name)).filter(isDirectory)

/* Get all the directories inside md-repo */
const dirArray =  getDirectories(mdRepo)
// console.log('dirArray: ', dirArray);


moveEverthing ()


readFiles(mdRepo)
function readFiles (dir) {
  fs.readdir(dir, function(err, filenames) {
    if (err) {
      console.log(err)
      return;
    }

    matchFiles = filenames.filter(x => x.match(regEx))
    console.log('matchFiles 24px svgs: ', matchFiles.length);

    try {
      for(let i = 0; i < matchFiles.length; i++) {
        const fileName = matchFiles[i]
        /* We clear the ic_ prefix and _24px sufix for the file name before moving them */
        const cleanIcon = fileName.replace('ic_', '').replace('_24px', '')
        moveThem(`${dir}/${matchFiles[i]}`, `svg/${cleanIcon}`)
      }
    } catch (error) {
      console.log(error)
    }
  })
}

/* Move files function that move one file from X to Y */
function moveThem(cur, dest) {
  (async () => {
    await moveFile(cur, dest)
})()
}

/* Move all files inside all directories from the array */
async function moveEverthing () {
  await dirArray.map(ele => {
    readFiles(`${ele}/svg/production`)
  })
}

